package fr.jovann.transhumanis.retrofit

import fr.jovann.transhumanis.models.Gare
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    @GET("getgares")
    fun getGare(): Call<List<Gare>>
}