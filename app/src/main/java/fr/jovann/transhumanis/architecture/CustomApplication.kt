package fr.jovann.transhumanis.architecture

import android.app.Application
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class CustomApplication: Application() {
    companion object {
        lateinit var instance: CustomApplication
        lateinit var mFirebaseAnalytics: FirebaseAnalytics
        lateinit var auth: FirebaseAuth
    }

    override fun onCreate() {
        super.onCreate()
        instance = this;
        auth = Firebase.auth;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }
}