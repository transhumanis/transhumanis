package fr.jovann.transhumanis.models

data class Gare(
    val idGare: String? = null,
    val label: String? = null

){
    override fun toString(): String {
        return label.toString();
    }
}