package fr.jovann.transhumanis.service



import android.R
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import fr.jovann.transhumanis.view.MainActivity


class MessagingService : FirebaseMessagingService() {

    var TAG = "message";
    private val NOTIFICATION_ID = 7
    private val NOTIFICATION_TAG = "FIREBASEOC"

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.from)
        if (remoteMessage.data.size > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)
        }

        if (remoteMessage.notification != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.notification!!.body)
            sendNotif(remoteMessage.notification!!.body.toString());

        }
    }

    public fun sendNotif(messageBody: String){
        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        val inboxStyle = NotificationCompat.InboxStyle()
        inboxStyle.setBigContentTitle("Transhumanis")
        inboxStyle.addLine(messageBody)

        val notificationBuilder =
            NotificationCompat.Builder(this, "a5")
                .setSmallIcon(R.drawable.ic_lock_lock)
                .setContentTitle("a")
                .setContentText("a")
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendingIntent)
                .setStyle(inboxStyle)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channelName: CharSequence = "Message provenant de Firebase"
        val importance = NotificationManager.IMPORTANCE_HIGH
        val mChannel = NotificationChannel("a5", channelName, importance)
        notificationManager.createNotificationChannel(mChannel)

        notificationManager.notify(NOTIFICATION_TAG, NOTIFICATION_ID, notificationBuilder.build());
    }
}