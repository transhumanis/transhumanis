package fr.jovann.transhumanis.view.account

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import fr.jovann.transhumanis.R
import fr.jovann.transhumanis.architecture.CustomApplication
import kotlinx.android.synthetic.main.activity_firebase_login.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_firebase_login)

        val context = this;

        sendRegister.setOnClickListener {
            //REGISTER
            val email: EditText = emailRegister
            val password: EditText = passwordRegister

            CustomApplication.auth.createUserWithEmailAndPassword(
                email.text.toString(),
                password.text.toString()
            )
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        Log.e("Register", "Success");
                        val builder = AlertDialog.Builder(context)
                        builder.setTitle("Création de votre compte effectuer")
                        builder.setPositiveButton("OK") {dialog, which ->
                            finish();
                        }
                        builder.show();
                    } else {
                        val builder = AlertDialog.Builder(context)
                        builder.setTitle("Erreur pendant la création de votre compte")
                        builder.setMessage(it.exception?.localizedMessage.toString());
                        builder.setPositiveButton("OK") {dialog, which ->
                            dialog.cancel()
                        }
                        builder.show();
                    }
                }

        }

        button.setOnClickListener {
            val email: EditText = editTextTextPersonName
            val password: EditText = editTextTextPassword


            CustomApplication.auth.signInWithEmailAndPassword(email.text.toString(),password.text.toString())
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        Log.e("Login", "Success");
                        finish();
                    } else {
                        val builder = AlertDialog.Builder(context)
                        builder.setTitle("Erreur pendant la connection à votre compte")
                        builder.setMessage(it.exception?.localizedMessage.toString());
                        builder.setPositiveButton("OK") {dialog, which ->
                            dialog.cancel()
                        }
                        builder.show();
                    }
                }
        }
    }
}