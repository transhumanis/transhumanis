package fr.jovann.transhumanis.view

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.LayoutRes
import androidx.appcompat.widget.Toolbar
import com.google.protobuf.Api
import fr.jovann.transhumanis.R
import fr.jovann.transhumanis.models.Gare
import fr.jovann.transhumanis.retrofit.ApiClient
import fr.jovann.transhumanis.view.AbstractActivity
import fr.jovann.transhumanis.view.account.LoginActivity
import kotlinx.android.synthetic.main.activity_firebase_login.*
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AbstractActivity() {

    var arrayList: ArrayList<Gare> = ArrayList()
    var adapter: GareAdapter? = null
    var adapter2: GareAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        topBarInit();

        val gareStart: AutoCompleteTextView = gareStart
        val gareEnd: AutoCompleteTextView = gareEnd2



        val gareRequest = ApiClient.apiService.getGare()


        var idGareStart: String ="";
        var idGareEnd: String ="";


        val context = this
        gareRequest.enqueue(object: Callback<List<Gare>>{
            override fun onResponse(call: Call<List<Gare>>, response: Response<List<Gare>>) {
                val all = response.body();
                all?.let {
                    for( tanStop in it) {

                        arrayList.add(tanStop)
                    }
                }

                adapter = GareAdapter(context,android.R.layout.simple_list_item_1,arrayList);
                adapter!!.setListener(object: IOnItemListener{
                    override fun onLongClick(dataToBePass: Array<String>) {
                        Log.e("e","e");
                    }

                    override fun onSingleClick(dataToBePass: Array<String>) {
                        Log.e("ee","ee");
                        gareStart.setText(dataToBePass.get(1))
                        idGareStart = dataToBePass.get(0);
                    }
                })
                adapter2 = GareAdapter(context,android.R.layout.simple_list_item_1,arrayList);
                adapter2!!.setListener(object: IOnItemListener{
                    override fun onLongClick(dataToBePass: Array<String>) {
                        Log.e("a","a");
                    }

                    override fun onSingleClick(dataToBePass: Array<String>) {
                        Log.e("aa","aa");
                        gareEnd.setText(dataToBePass.get(1))
                        idGareEnd = dataToBePass.get(0);
                    }
                })
                gareStart.setAdapter(adapter)
                gareStart.threshold = 3

                gareEnd.setAdapter(adapter2)
                gareEnd.threshold = 3

            }

            override fun onFailure(call: Call<List<Gare>>, t: Throwable) {
                Log.e("a", t.message.toString())
            }
        })

        sendSearch.setOnClickListener {
            Log.e("s",idGareStart);
            Log.e("e",idGareEnd);
            if(idGareEnd != idGareStart){
                val intent = Intent(context, ResultActivity::class.java);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            } else {
                val builder = AlertDialog.Builder(context)
                builder.setTitle("Erreur")
                builder.setMessage("Les gares ne peuvent pas être identique");
                builder.setPositiveButton("OK") {dialog, which ->
                    dialog.cancel()
                }
                builder.show();
            }
        }

    }


    inner class GareAdapter(context: Context, @LayoutRes private val layoutResource: Int, private val allPois: List<Gare>):
        ArrayAdapter<Gare>(context, layoutResource, allPois),
        Filterable {

        private var mListener: IOnItemListener? = null

        fun setListener(listener : IOnItemListener) {
            mListener = listener
        }

        private var mPois: List<Gare> = allPois

        override fun getCount(): Int {
            return mPois.size
        }

        override fun getItem(p0: Int): Gare? {
            return mPois.get(p0)
        }


        override fun getItemId(p0: Int): Long {
            // Or just return p0
            return mPois.get(p0).idGare!!.toLong()
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val view: TextView = convertView as TextView? ?: LayoutInflater.from(context).inflate(layoutResource, parent, false) as TextView
            view.text = "${mPois[position].label}";

            view.setOnClickListener{
                var colors_3: Array<String> = arrayOf("","")
                colors_3.set(0, mPois[position].idGare.toString())
                colors_3.set(1,mPois[position].label.toString());
                mListener?.onSingleClick(colors_3);
            }


            return view
        }



        override fun getFilter(): Filter {
            return object : Filter() {
                override fun publishResults(charSequence: CharSequence?, filterResults: Filter.FilterResults) {
                    mPois = filterResults.values as List<Gare>
                    notifyDataSetChanged()
                }

                override fun performFiltering(charSequence: CharSequence?): Filter.FilterResults {
                    val queryString = charSequence?.toString()?.toLowerCase()

                    val filterResults = Filter.FilterResults()
                    filterResults.values = if (queryString==null || queryString.isEmpty())
                        allPois
                    else
                        allPois.filter {
                            it.label!!.toLowerCase().contains(queryString)
                        }
                    return filterResults
                }
            }
        }
    }

    interface IOnItemListener {
        fun onLongClick(dataToBePass : Array<String>)
        fun onSingleClick(dataToBePass : Array<String>)
    }
}

