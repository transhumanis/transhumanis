package fr.jovann.transhumanis.view

import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import fr.jovann.transhumanis.R
import fr.jovann.transhumanis.architecture.CustomApplication
import fr.jovann.transhumanis.view.account.LoginActivity

open class AbstractActivity : AppCompatActivity(){

    fun topBarInit(){
        val toolbar = findViewById<Toolbar>(R.id.toolbar);
        setSupportActionBar(toolbar)



    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu,menu);
        val currentUser = CustomApplication.auth.currentUser;
        if(currentUser != null){
            menu?.getItem(0)?.setVisible(false);
            menu?.getItem(1)?.setVisible(true);
            menu?.getItem(2)?.setVisible(true);
        }
        return true;
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here.
        val id = item.itemId

        if(id == R.id.login){
            val intent = Intent(this,LoginActivity::class.java);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            return true;
        }
        if(id == R.id.myAccount){
            return true;
        }
        if(id == R.id.logout){
            CustomApplication.auth.signOut();
            recreate();
            return  true;
        }


        return super.onOptionsItemSelected(item)

    }

}